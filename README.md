This project is archived, activity is being migrated to https://gitlab.com/wescalefr-oss/terraform-modules/terraform-aws-subnet-share for renaming purpose.     
Sources are kept for active stacks which would reference it. Please update your paths.

# AWS Subnet Share

This module implement [AWS RAM](https://aws.amazon.com/ram/) to share a specified subnet with an organization or other accounts.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Providers

| Name | Version |
|------|---------|
| aws | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| accounts\_arn | Share the subnet with this list of accounts ARN. | `list(string)` | `[]` | no |
| allow\_externals | Allow sharing with externals account (bool). | `string` | `"false"` | no |
| organization\_arn | ID of the organization (Sharing must be enabled at organization level : https://console.aws.amazon.com/ram/home#Settings). | `string` | `"false"` | no |
| share\_name | Name of the resource share. | `string` | `"main"` | no |
| subnet\_id | ID of the shared subnet. | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| share\_id | ARN of the resource share. |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->